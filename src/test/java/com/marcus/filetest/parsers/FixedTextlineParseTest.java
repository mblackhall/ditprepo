package com.marcus.filetest.parsers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FixedTextlineParseTest {

    @Test
    public void shouldParseFixedLine() {
        String line = "Doe, JohnxxxxxxxLong street 100xx1050abxxxx0800 12345    2500             19750101";
        String[] columns = new ParseTxtFixedColumns().retrieveColumns(line);
        assertEquals(6,columns.length);
        assertEquals("19750101", columns[5]);
    }
}
t