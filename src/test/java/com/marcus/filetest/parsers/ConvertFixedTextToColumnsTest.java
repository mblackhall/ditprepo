package com.marcus.filetest.parsers;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;


public class ConvertFixedTextToColumnsTest {
    @Test
    public void headerAndRowsExtractedOkeTest() {
        String filename = "file.txt";
        InputStream is = getClass().getResourceAsStream("/" + filename);
        assertNotNull("Test file is missing", is);
        Reader reader = new InputStreamReader(is);
        FileAsColumns fileAsColumns = new FileAsColumns(reader,filename);

        try {
            fileAsColumns.convertToColumns();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        String [] headerCols = fileAsColumns.getHeaderLine();
        assertNotNull(headerCols);
        assertEquals(6,headerCols.length);
        assertEquals(headerCols[0],"Name");
        assertEquals(headerCols[5],"Birthday");
        assertEquals(4,fileAsColumns.getDetailLines().size());

    }

}
