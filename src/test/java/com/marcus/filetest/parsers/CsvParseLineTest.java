package com.marcus.filetest.parsers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CsvParseLineTest {

    @Test
    public void allColumnsShouldMatch() {

        String line = "\"Rabbit, Roger\", Acmø straße 2, 1002 cc, 0900 - 12349, 1237.5, 20/08/1930";
        String[] columns = new ParseCsvLine().retrieveColumns(line);
        assertEquals(columns.length, 6);
        assertEquals("Rabbit, Roger", columns[0]);
        assertEquals("Acmø straße 2", columns[1]);
        assertEquals("1002 cc", columns[2]);
        assertEquals("0900 - 12349", columns[3]);
        assertEquals("1237.5", columns[4]);
        assertEquals("20/08/1930", columns[5]);
    }

    @Test
    public void lastColumnWithQuotesIsCorrect(){
        String line = "\"Rabbit, Roger\", Acmø straße 2, 1002 cc, 0900 - 12349, 1237.5,\"20/08/1930\"";
        String[] columns = new ParseCsvLine().retrieveColumns(line);
        assertEquals("20/08/1930",columns[5]);
    }

    @Test
    public void shouldHandleEmptyColumn(){
        String line = "\"Rabbit, Roger\", Acmø straße 2,, 0900 - 12349, 1237.5,\"20/08/1930\"";

        String[] columns = new ParseCsvLine().retrieveColumns(line);
        assertEquals(columns.length, 6);
        assertEquals("Rabbit, Roger", columns[0]);
        assertEquals("Acmø straße 2", columns[1]);
        assertEquals("", columns[2]);
        assertEquals("0900 - 12349", columns[3]);
        assertEquals("1237.5", columns[4]);
        assertEquals("20/08/1930", columns[5]);
    }

    @Test
    public void emptyLineReturnsNull(){
        String line = "";

        String[] columns = new ParseCsvLine().retrieveColumns(line);
    }
}
