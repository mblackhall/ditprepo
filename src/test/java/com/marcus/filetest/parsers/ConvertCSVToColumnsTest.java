package com.marcus.filetest.parsers;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import static org.junit.Assert.*;

public class ConvertCSVToColumnsTest {

    @Test
    public void csvFileConvertedToColumnsCorrectly() {
        String filename = "file.csv";
        InputStream is = getClass().getResourceAsStream("/" + filename);
        assertNotNull("Test file is missing", is);
        Reader reader = new InputStreamReader(is);
        FileAsColumns fileAsColumns = new FileAsColumns(reader,filename);
        String [] rows;
        try {
            fileAsColumns.convertToColumns();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertNotNull(fileAsColumns.getHeaderLine());
        assertNotNull(fileAsColumns.getDetailLines());
        assertEquals( 4,fileAsColumns.getDetailLines().size());

    }

}
