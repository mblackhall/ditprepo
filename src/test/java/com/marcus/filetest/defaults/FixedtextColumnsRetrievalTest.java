package com.marcus.filetest.defaults;

import com.marcus.filetest.defauts.FixedtextSetup;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FixedtextColumnsRetrievalTest {

    @Test
    public void shouldLoadFixedTextProperties(){

        assertEquals(FixedtextSetup.getFixedColumnLengths().length, 6);
    }
}
