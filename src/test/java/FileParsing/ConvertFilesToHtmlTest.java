package FileParsing;

import com.marcus.filetest.parsers.FileAsColumns;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.fail;


public class ConvertFilesToHtmlTest {


    @Test
    public void produceHtmlFilesForTextAndCsv() {

       try {
           convertFileToHtml("file.txt", "filefixedtext.html");
           convertFileToHtml("file.csv", "filecsv.html");
       } catch (Exception e ){
           fail();
       }

    }


    private void convertFileToHtml(String inputFilename, String outputFilename) throws Exception {

        InputStream is = getClass().getResourceAsStream("/" + inputFilename);

        Reader reader = new InputStreamReader(is);
        FileAsColumns fileAsColumns = new FileAsColumns(reader, inputFilename);

        try {
            fileAsColumns.convertToColumns();
        } catch (IOException e) {

        }

        HtmlProducer.produceAccountHtmlFromColumnsUsingXslt(new PrintWriter(outputFilename), fileAsColumns.getDetailLines());

    }

}
