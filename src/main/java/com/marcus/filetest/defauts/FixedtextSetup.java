package com.marcus.filetest.defauts;

import java.io.IOException;
import java.util.Properties;

public class FixedtextSetup {

    public static final String FIXED_LENGTHS = "fixedcolumnlengths";


    protected static Properties fixedtextcolumnslengths = new Properties();

    static {
        try {
            fixedtextcolumnslengths.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("fixedtextpositions.properties"));


        } catch (IOException e) {
            throw new IllegalStateException("loading of fixed text columns length properties failed", e);
        }
    }

    public static int [] getFixedColumnLengths() {

        String [] strFixedLengths = fixedtextcolumnslengths.getProperty(FIXED_LENGTHS).split(",");
        int [] fixedLengths = new int[strFixedLengths.length];
        int i = 0;
        for ( String colLength : strFixedLengths ){
            fixedLengths[i] = Integer.parseInt(colLength);
            i++;
        }
        return fixedLengths;
    }


}
