package com.marcus.filetest.parsers;


import java.util.ArrayList;
import java.util.List;

public class ParseCsvLine implements ParseLine {

    char DEFAULT_SEPARATOR = ',';
    char DEFAULT_QUOTE = '"';


    public String[] retrieveColumns(String line) {

        if (line == null || line.trim().isEmpty()) {
            return null;
        }

        List<String> columns = new ArrayList<>();
        StringBuilder column = new StringBuilder();
        char c;
        boolean searchEndQuote = false;
        for (int i = 0; i < line.length(); i++) {
            c = line.charAt(i);
            // check if first character is a quote
            if (c == DEFAULT_QUOTE) {
                searchEndQuote = !searchEndQuote;
                continue;
            }

            if (c == DEFAULT_SEPARATOR) {

                if (searchEndQuote) {
                    column.append(c);
                    continue;
                }

                columns.add(column.toString().trim());
                column.setLength(0);
                continue;
            }

            column.append(c);
        }
        // add the last column
        columns.add(column.toString().trim());

        return columns.toArray(new String[columns.size()]);

    }


}
