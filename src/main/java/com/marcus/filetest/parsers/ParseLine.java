package com.marcus.filetest.parsers;


public interface ParseLine {

    String[] retrieveColumns(String line);
}
