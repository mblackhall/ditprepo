package com.marcus.filetest.parsers;


public class ParselineFactory {

    private static final ParselineFactory INSTANCE = new ParselineFactory();

    private ParselineFactory() {
    }

    public static ParselineFactory getInstance() {
        return INSTANCE;
    }

    /**
     * @param filename
     * @return parser to use base on the file extension. Null is returned if file extension is not recognised
     */
    public ParseLine determineParser(String filename) {

        int startExt = filename.lastIndexOf(".");
        if (startExt == -1 || startExt == (filename.length() - 1)) {
            return null;
        }

        ParseLine parseline;
        String fileExtension = filename.substring(startExt + 1).toLowerCase();
        switch (fileExtension) {
            case "csv":
                parseline = new ParseCsvLine();
                break;
            case "txt":
                parseline = new ParseTxtFixedColumns();
                break;

            default:
                parseline = null;
        }


        return parseline;
    }


}
