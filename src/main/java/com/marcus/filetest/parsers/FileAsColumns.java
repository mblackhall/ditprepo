package com.marcus.filetest.parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;


public class FileAsColumns {


    private String[] headerLine;
    private List<String[]> detailLines = new ArrayList<>();
    protected Reader reader;
    private ParseLine parseLine;

    public FileAsColumns(Reader reader, String filename) {
        this.reader = reader;
        parseLine = ParselineFactory.getInstance().determineParser(filename);
    }

    public void convertToColumns() throws IOException {

        BufferedReader bufferedReader;
        if (reader instanceof BufferedReader) {
            bufferedReader = (BufferedReader) reader;
        } else {
            bufferedReader = new BufferedReader(reader);
        }

        String line;
        String[] lineAsCols;
        boolean header = true;
        while ((line = bufferedReader.readLine()) != null) {


            lineAsCols = parseLine.retrieveColumns(line);
            // blank lines will be ignored
            if (lineAsCols == null) {
                continue;
            }
            if (header) {
                headerLine = lineAsCols;
                header = false;
                continue;
            }
            detailLines.add(lineAsCols);
        }

    }

    public String[] getHeaderLine() {
        return headerLine;
    }

    public List<String[]> getDetailLines() {
        return detailLines;
    }

}
