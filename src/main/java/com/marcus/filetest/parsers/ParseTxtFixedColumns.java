package com.marcus.filetest.parsers;


import com.marcus.filetest.defauts.FixedtextSetup;

public class ParseTxtFixedColumns implements ParseLine {

    @Override
    public String[] retrieveColumns(String line) {

        if ( line == null || line.trim().isEmpty() ){
            return null;
        }

        int [] colLengths = FixedtextSetup.getFixedColumnLengths();
        String [] columns = new String[colLengths.length];
        int startPosition = 0;
        for (int i=0;i<colLengths.length;i++){
            columns[i] = line.substring(startPosition,startPosition+colLengths[i]).trim();
            startPosition += colLengths[i];

        }
        return columns;
    }

}
