package com.marcus.filetest.beans;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="accounts")
public class AccountList {


private List<Account> accounts = new ArrayList<>();

    @XmlElement(name="account")
    public List<Account> getAccounts() {
        return accounts;
    }
}
