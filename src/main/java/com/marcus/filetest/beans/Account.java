package com.marcus.filetest.beans;


public class Account {

    private String name;
    private String  address;
    private String  zipcode;
    private String  phone;
    private String balance;
    private String birthday;

    public Account(){
    }

    public Account(String [] line ){

        name = line[0];
        address = line [1];
        zipcode = line[2];
        balance = line [3];
        phone = line [4];
        birthday = line[5];

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }




}
