package FileParsing;

import com.marcus.filetest.beans.Account;
import com.marcus.filetest.beans.AccountList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.Writer;
import java.util.List;


public class HtmlProducer {

    public static final String ACCOUNT_XSL_TEMPLATE = "/accounttable.xsl";
    public static void produceAccountHtmlFromColumnsUsingXslt(Writer writer, List<String[]> lines) throws Exception {
        AccountList accountList = new AccountList();


        for (String[] line : lines) {

            accountList.getAccounts().add(new Account(line));
        }

        // Create Transformer
        TransformerFactory tf = TransformerFactory.newInstance();
        InputStream is = HtmlProducer.class.getResourceAsStream(ACCOUNT_XSL_TEMPLATE);

        StreamSource xslt = new StreamSource(is);

        Transformer transformer = null;
        try {
            transformer = tf.newTransformer(xslt);


            // Source
            JAXBContext jc = JAXBContext.newInstance(AccountList.class);
            JAXBSource source = new JAXBSource(jc, accountList);
            StreamResult result = new StreamResult(writer);

            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            throw new Exception(e);
        }


    }

}
