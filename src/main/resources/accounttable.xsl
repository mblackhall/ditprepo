<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <style>

                    th {
                         text-align: left;
                         background-color: lightgray;

                    }

                </style>

            </head>
            <body>
                <h2>Accounts</h2>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Zip-code</th>
                        <th>Phone</th>
                        <th>Account Balance</th>
                        <th>Birthday</th>
                    </tr>
                    <xsl:for-each select="accounts/account">
                        <tr>
                            <td>
                                <xsl:value-of select="name" />
                            </td>
                            <td>
                                <xsl:value-of select="address" />
                            </td>
                            <td>
                                <xsl:value-of select="zipcode" />
                            </td>
                            <td>
                                <xsl:value-of select="phone" />
                            </td>
                            <td>
                                <xsl:value-of select="balance" />
                            </td>
                            <td>
                                <xsl:value-of select="birthday" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>